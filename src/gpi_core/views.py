from django.shortcuts import render, redirect
from django.http import JsonResponse
from gpi_core.models import *
from gpi_core.forms import GPIForm, AgencyForm, TaskForm, CommentForm
from django.core.exceptions import PermissionDenied

# Create your views here.


def home(request):
    list_gpi = GPI.objects.all()
    return render(request, 'gpi_core/list_gpi.html', {'list_gpi': list_gpi})


def sort_gpi(request, criteria: str, order: str):
    criteria = criteria.split("_link")[0]
    if criteria in ['name', 'metropolis', 'address', 'zip_code']:
        criteria = 'agency__' + criteria
    if order == 'desc':
        criteria = "-"+criteria
    gpi_set = GPI.objects.order_by(criteria)
    # appel d'une vue dans une vue + nom des variables et nom de la fonction
    gpi_dict = get_all_fields_as_list(gpi_set)
    return JsonResponse({"gpi_sorted": list(gpi_dict)})


def create_gpi(request):
    form = GPIForm()
    if request.method == 'POST':
        form = GPIForm(request.POST)
        if form.is_valid():
            form.save()
    #         il doit y avoir un moyen de set 'gpi_core' comme url par défaut de l'application
    # on peut utiliser locals()
    return render(request, 'gpi_core/create_gpi.html', {'form': form})


def create_agency(request):
    form = AgencyForm()
    if request.method == 'POST':
        form = AgencyForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request, 'gpi_core/create_agency.html', {'form': form})


def search_gpi(request, string_searched: str):
    matching_gpi_set = GPI.objects.filter(agency__name__icontains=string_searched)
    matching_gpi_dict = get_all_fields_as_list(matching_gpi_set)
    return JsonResponse({"gpi_found": list(matching_gpi_dict)})


def get_all_gpi(request):
    gpi_set = GPI.objects.all()
    gpi_dict = get_all_fields_as_list(gpi_set)
    return JsonResponse({"all_gpi": list(gpi_dict)})


def view_gpi(request, gpi_id):
    gpi = GPI.objects.get(id=gpi_id)
    if request.method == 'GET':
        computed_tasks = gpi.compute_v_ranks()
    return render(request, 'gpi_core/view_gpi.html', {'gpi': gpi,
                                                      'computed_tasks': computed_tasks,
                                                      'categories': Category.objects.all()
                                                      })


def view_task(request, gpi_id, task_id=None):
    template = "gpi_core/view_task.html"
    task = None
    context = {}
    gpi = GPI.objects.get(pk=gpi_id)
    comment_form = CommentForm()
    if task_id is not None:
        task = Task.objects.get(pk=task_id)
    else:
        if request.method == "GET":
            form = TaskForm(gpi)
        else:
            form = TaskForm(gpi, request.POST)
    if task is not None:
        if request.method == "GET":
            form = TaskForm(gpi=gpi, instance=task)
            form.initial["duration"] = task.duration.days
        else:
            form = TaskForm(gpi, request.POST, instance=task)
        context.update({"task": task})
    context.update({"task_creation_form": form,
                    "comment_form": comment_form,
                    "gpi": gpi})

    if request.method == "GET":
        if task is not None and task in form.fields["come_after"].queryset and task in form.fields["come_before"].queryset:
            form.fields["come_before"].queryset = gpi.tasks.exclude(pk=task.id)
            form.fields["come_after"].queryset = gpi.tasks.exclude(pk=task.id)
        return render(request, template_name=template, context=context)
    if form.is_valid():
        form.save()
        gpi.compute_earliest_dates()
        gpi.compute_latest_dates()
        gpi.compute_margins()
        gpi.compute_v_ranks()
    return render(request, template_name=template, context=context)


def create_comment(request, task_id):
    task = Task.objects.get(pk=task_id)
    author = InternalContributor.objects.first()  # TODO: relier à l'utilisateur actuellement connecté
    if task is not None:
        if request.method == "POST":
            comment_form = CommentForm(request.POST)
            if comment_form.is_valid():
                comment_form.save()
                return render(request=request, template_name="gpi_core/view_task.html")


def delete_comment(request, task_id, comment_id):
    comment = Comment.objects.get(pk=comment_id)
    task = Task.objects.get(pk=task_id)
    task_creation_form = TaskForm(instance=task)
    comment_form = CommentForm(request)
    context = {"task": task, "task_creation_form": task_creation_form, "comment_form": comment_form}
    if comment.task == task:
        comment.delete()
        return render(request, template_name="gpi_core/view_task.html", context=context)
    else:
        raise PermissionDenied


def update_comment(request, task_id, comment_id):
    comment = Comment.objects.get(pk=comment_id)
    task = Task.objects.get(pk=task_id)
    if comment.task == task:
        comment_form = CommentForm(request, instance=comment)
        if comment_form.is_valid():
            comment_form.save()
            return render(request, template_name="gpi_core/view_task.html")


def delete_task(request, gpi_id, task_id):

    gpi = GPI.objects.get(pk=gpi_id)
    task = Task.objects.get(pk=task_id)

    if task is not None and task.gpi.id == gpi.id:
        task.delete()
        gpi.compute_earliest_dates()
        gpi.compute_latest_dates()
        gpi.compute_margins()
    return redirect('view_gpi', gpi_id=task.gpi.id)


def get_all_fields_as_list(gpi_set):
    # gpi_dict = une liste,
    # parcours de liste par list[index] plutôt que par élément,
    # c'est de la serialisation? Si oui, utiliser from django.core import serializers ou obj.__dict__
    gpi_dict = list(gpi_set.values("id", "definitive_signing_date", "protocol_signing_date", "status"))
    for gpi_counter in range(len(gpi_dict)):
        gpi_dict[gpi_counter]["agency_name"] = gpi_set[gpi_counter].agency.name
        gpi_dict[gpi_counter]["agency_address"] = gpi_set[gpi_counter].agency.address
        gpi_dict[gpi_counter]["agency_zip_code"] = gpi_set[gpi_counter].agency.zip_code
        gpi_dict[gpi_counter]["agency_metropolis"] = gpi_set[gpi_counter].agency.metropolis
    return gpi_dict
