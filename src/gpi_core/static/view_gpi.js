const months_list = ['janv.', 'fevr.', 'mars', 'avr.', 'mai.', 'juin.', 'juill.', 'août', 'sept.', 'oct.', 'nov.', 'dec.'];
let task_list = document.querySelectorAll(".task");
let calendar_container = document.querySelector(".calendar-container");
let tasks_container = document.querySelector(".tasks-container");
get_displayed_days(task_list, calendar_container, tasks_container);
position_tasks(task_list, tasks_container);
draw_links();
window.addEventListener('resize', draw_links);
//task_list.forEach((task)=>task.addEventListener('click', (e)=>console.log(e)));

/**
 * fonction en charge de l'initialisation de la timeline. Cette méthode prend en compte les dates de début au plus tôt des
 * tâches du gpi renvoyées par la vue. Améliorations à mettre en place :
 * - mettre un nombre de jours avant et après pour avoir des marges à droite et à gauche.
 * - gérer le cas où l'intervalle de jours à afficher est trop élevé et ne plus afficher toutes les dates de chaque jour,
 * mais plutôt 1/2 puis 1/3, 1/4 ... Comment le gérer : MediaQueries, règles javascript ?
 */
function get_displayed_days (task_list, calendar_container, tasks_container) {
    let earliest_start_dates_array = [];
    let earliest_end_dates_array = [];
    task_list.forEach((value) => {
        earliest_start_dates_array.push(new Date(value.dataset.earliestStartDate))
    });
    task_list.forEach((value) => {
        earliest_end_dates_array.push(new Date(value.dataset.earliestEndDate))
    });
    let first_day = new Date(Math.min(...earliest_start_dates_array));
    let last_day = new Date(Math.max(...earliest_end_dates_array));
    let today = new Date(new Date(Date.now()).toDateString());
    let start_date = new Date(first_day);
    let year_container = create_year_container(start_date, calendar_container);
    let month_container = create_month_container(start_date, year_container);
    year_container.date = new Date(start_date);
    month_container.date = new Date(start_date);
    let day_container = create_day_container(start_date, month_container);
    let column_wrapper = create_column_wrapper(tasks_container);
    day_container.dataset.date = start_date.toDateString();
    let index_loop = 0;
    while (start_date < last_day) {
        start_date.setDate(start_date.getDate() + 1);
        if (start_date.getDay() != 0 && start_date.getDay() != 6) {
            if (year_container.date.getFullYear() != start_date.getFullYear()) {
                year_container = create_year_container(start_date, calendar_container);
                year_container.date = start_date;
            }
            if (month_container.date.getMonth() != start_date.getMonth()) {
                month_container = create_month_container(start_date, year_container);
                month_container.date = start_date;
            }
            index_loop++;
            let new_day = create_day_container(start_date, month_container);
            column_wrapper = create_column_wrapper(tasks_container);
            if(today.getTime() === start_date.getTime())
            {
                new_day.classList.add("current-day");
                column_wrapper.classList.add("current-day");
            }

            new_day.dataset.date = start_date.toString();
        }
    }
    document.querySelectorAll('.year-container').forEach((value) => {
        let columns_months_string = ""
        value.querySelectorAll('.month-container').forEach(
            (month) => {
                columns_months_string += `${month.querySelectorAll('.day-container').length}fr `.toString();
            })
        value.style.gridTemplateColumns = columns_months_string;
        value.style.gridColumn = `1 / span ${value.querySelectorAll('.day-container').length}`.toString();
    });
    document.querySelectorAll('.month-container').forEach((value, index) => {
        value.style.gridTemplateColumns = `repeat(${value.querySelectorAll('.day-container').length} , 1fr)`.toString();
    });
}

/**
 * fonction positionnant les tâches dans l'élément task_container afin d'établir leurs colonnes
 * et leurs lignes de début et de fin en fonction de leurs dates de début au plus tôt et de leur durée
 * et aussi en fonction de la hauteur de la tâche qui aura au préalable été calculée côté backend
 * @param task_list
 * @param tasks_container
 */
function position_tasks (task_list, tasks_container) {

    let dates_array = []
    document.querySelectorAll(".day-container").forEach((value) => {
        (dates_array.push(new Date(value.dataset.date).getTime()));
    });
    let max_height = 0;
    task_list.forEach((value) => {
        let duration = parseInt(value.dataset.duration, 10);
        let height = parseInt(value.dataset.height, 10);
        height > max_height ? max_height = height : max_height=max_height;
        let column_start = dates_array.indexOf(new Date(value.dataset.earliestStartDate).getTime()) + 1;
        let column_end = column_start + duration;
        let row_start = 1 + height;
        let row_end = row_start + 1;
        value.style.gridArea = `${row_start} / ${column_start} / ${row_end} / ${column_end}`.toString();
        value.draggable = true;
        tasks_container.appendChild(value);
    });
    tasks_container.style.gridColumn = `1 / span ${document.querySelectorAll('.day-container').length}`.toString();
    tasks_container.style.gridTemplateRows = `repeat(${max_height +1}, 1fr)`.toString();
    tasks_container.style.gridTemplateColumns = `repeat(${document.querySelectorAll('.day-container').length}, 1fr)`.toString();
}
function draw_links(e)
{
    if(e != null)
    {
        document.querySelectorAll("canvas").forEach((value)=> value.remove());
    }
    let tasks_with_pred = document.querySelectorAll(".task[data-pred-list]");
    tasks_with_pred.forEach((task_suc)=>
    {
        let pred_list = task_suc.dataset.predList.split("-");
        pred_list.forEach((pred) =>
        {
            let task_pred = document.querySelector(`#task_${pred}`.toString());
            let link = document.createElement("canvas");
            link.width = task_suc.getBoundingClientRect().left - task_pred.getBoundingClientRect().right;
            let top_bottom_tasks = task_pred.getBoundingClientRect().y > task_suc.getBoundingClientRect().y ? [task_suc, task_pred] : [task_pred, task_suc];
            link.height = top_bottom_tasks[1].getBoundingClientRect().bottom - top_bottom_tasks[0].getBoundingClientRect().top;
            link.style.left = `${task_pred.getBoundingClientRect().width}px`.toString();
            let ctx = link.getContext("2d");
            if(top_bottom_tasks[1] == task_pred)
            {
                link.style.top = `${link.height*-1 + task_pred.getBoundingClientRect().height}px`;
                ctx.moveTo(0, link.height - task_pred.getBoundingClientRect().height/2);
                ctx.lineTo(link.width, task_suc.getBoundingClientRect().height/2 );
            }
            else
            {
                ctx.moveTo(0, task_pred.getBoundingClientRect().height/2);
                ctx.lineTo(link.width, link.height - task_suc.getBoundingClientRect().height/2);

            }
            if(task_pred.dataset.margin.indexOf("day") === -1 && task_suc.dataset.margin.indexOf("day") === -1)
            {
                ctx.strokeStyle = "red";

            }
            ctx.stroke()
            task_pred.appendChild(link);

        })
    })
}
/**
 * @param new_date
 * @param month_container
 * @return day_container
 */
function create_day_container (new_date, month_container) {
    let day_template = document.querySelector('#day');
    let day_cloned = document.importNode(day_template.content, true);
    let day_container = day_cloned.querySelector('.day-container');
    month_container.appendChild(day_container);
    day_container.querySelector('.day-cell').innerText = new_date.getDate();
    return day_container;
}

/**
 *
 * @param new_date
 * @param year_container
 * @return month_container
 */
function create_month_container (new_date, year_container) {
    let month_template = document.querySelector('#month');
    let month_cloned = document.importNode(month_template.content, true);
    let month_container = month_cloned.querySelector('.month-container');
    year_container.appendChild(month_container);
    month_container.querySelector('.month-cell').innerText = months_list[new_date.getMonth()];
    return month_container;
}

/**
 *
 * @param new_date
 * @param calendar_container
 * @return year_container
 */
function create_year_container (new_date, calendar_container) {
    let year_template = document.querySelector('#year');
    let year_cloned = document.importNode(year_template.content, true);
    let year_container = year_cloned.querySelector('.year-container');
    calendar_container.appendChild(year_container)
    year_container.querySelector('.year-cell').innerText = new_date.getFullYear();
    return year_container;
}
function create_column_wrapper(task_container)
{
    let column_template = document.querySelector('#column');
    let column_cloned = document.importNode(column_template.content, true);
    let column_wrapper = column_cloned.querySelector('.column-wrapper');
    task_container.appendChild(column_wrapper);
    let nb_columns = tasks_container.querySelectorAll('.column-wrapper').length;
    column_wrapper.style.gridRow=`1/-1`;
    column_wrapper.style.gridColumn=`${nb_columns} / ${nb_columns+1}`.toString();
    return column_wrapper;
}