let come_after_container = document.querySelector('#id_come_after');
let come_before_container = document.querySelector('#id_come_before');
let search_bar_list = document.querySelectorAll(".search-field input");
search_bar_list.forEach((search_bar) => search_bar.addEventListener("input", search_field_changed));
document.querySelectorAll("input[type=checkbox]").forEach((task) =>
{
    task.addEventListener("click", (e)=>task_checked(e))
});
/**
 * fonction provoquant soit la restauration, soit la suppression d'une tâche selon la nature de
 * l'état de la checkbox à l'origine du déclenchement de l'évènement.
 * @param e
 * @param array_removed_tasks
 */
function task_checked (e) {
    let parent = come_before_container.contains(e.target) ? come_before_container : come_after_container;
    let earliest_start_date_parent = document.querySelector("#id_earliest_start_date").parentElement;
    let earliest_start_date = earliest_start_date_parent.querySelector("input");

    if (e.target.checked === true) {
        if(parent === come_before_container && earliest_start_date_parent.style.display !== "none")
        {
            earliest_start_date_parent.style.display = "none";
            earliest_start_date.old_date = earliest_start_date.value;
            earliest_start_date.value = "";
            earliest_start_date.disabled = true;
        }
        task_deletion(e);
    }
    else {
        //On supprime la tâche de la liste courante et on la restaure dans l'autre.
        //De plus, si tous les prédécesseurs ont été désélectionnés, on restaure la date de début au plus tôt avec son ancienne valeur.
        if(parent === come_before_container && parent.querySelectorAll('input[type="checkbox"]:checked').length == 0)
        {
            earliest_start_date_parent.style.display = "block";
            earliest_start_date.disabled = false;
            earliest_start_date.value = earliest_start_date.old_date;

        }
        task_restoration(e);
    }
}

/**
 * fonction permettant la restauration d'une tâche lorsque la tâche correspondante a été désélectionnée de la liste
 * correspondante
 * remarque : sans doute inutilement compliqué, il suffirait seulement de mettre un display à none
 * simplification effectuée
 * implémentation de la suppression des successeurs immédiats d'une tâche de la liste des prédécesseurs si elle a été ajoutée dans la liste des prédécesseurs
 * @param e
 * @param array_removed_tasks
 */
function task_restoration (e) {
    let other_elements_to_restore = [];
    let current_prefix = e.target.id.toString().indexOf("before") != -1 ? "id_come_before_" : "id_come_after_";
    let reciprocal_prefix = e.target.id.toString().indexOf("before") != -1 ? "id_come_after_" : "id_come_before_";
    let reciprocal_id = e.target.id.replace(current_prefix, reciprocal_prefix);
    let element_to_restore = document.querySelector(`label[for=${reciprocal_id}]`);
    other_elements_to_restore = come_after_container.contains(element_to_restore) ? get_all_predecessors(element_to_restore.parentElement) : get_all_successors(element_to_restore.parentElement);
    element_to_restore.querySelector("input").disabled = false;
    element_to_restore.style.display = "block";
    other_elements_to_restore.forEach((task)=>
    {
        task.style.display = "block";
        task.disabled = false;
    });

}

/**
 * Fonction pour obtenir les prédécesseurs directs d'une tâche
 * @param task
 */
function get_predecessors(task)
{

    let parent_container = come_before_container.contains(task) ? come_before_container : come_after_container;
    let pred_list = [];
    let pred_id_list = task.dataset.predList.split("-");
    pred_id_list.forEach((value)=>
    {
        if(value != null && value)
        {
            let pred = parent_container.querySelector(`div[data-task-id="${value}"]`);
            pred.was_explored = false;
            pred_list.push(pred);
        }

    });
    return pred_list;
}

/**
 * Fonction pour obtenir les successeurs directs d'une tâche
 * @param task
 */
function get_successors(task)
{
    let parent_container = come_before_container.contains(task) ? come_before_container : come_after_container;
    let suc_list = [];
    let suc_id_list = task.dataset.sucList.split("-");
    suc_id_list.forEach((value)=> {
            if(value != null && value)
            {
                let suc = parent_container.querySelector(`div[data-task-id="${value}"]`);
                suc.was_explored = false;
                suc_list.push(suc);
            }
        }
    );
    return suc_list;
}

/**
 * Fonction pour obtenir l'ensemble des successeurs d'une tâche
 * @param task
 */
function get_all_successors(task)
{
    let result = get_successors(task);
    for(let res of result)
    {
        if(res != null && !res.was_explored)
        {
            let suc_list = get_successors(res);
            suc_list.forEach((suc_elem) =>
            {
                result.push(suc_elem);
                suc_elem.was_explored = false;
            });
            res.was_explored = true;
        }
    }
    return result;
}

/**
 * Fonction pour obtenir l'ensemble des prédécesseurs d'une tâche
 * @param tasks
 */
function get_all_predecessors(task)
{
    let result = get_predecessors(task);
    for(let res of result)
    {
        if(res!= null && !res.was_explored)
        {
            let pred_list = get_all_predecessors(res);
            pred_list.forEach((pred_elem) =>{
                result.push(pred_elem);
                pred_elem.was_explored = false;});
            res.was_explored = true;
        }
    }
    return result;
}
/**
 * fonction permettant la suppression d'une tâche de la liste des précédents lorsqu'elle a
 * été sélectionnée dans la liste des suivants et réciproquement
 * remarque : sans doute inutilement compliqué, il suffirait seulement de mettre un display à none
 * @param e
 * @param array_removed_tasks
 */
function task_deletion (e, array_removed_tasks) {
    let removed_task;
    let other_tasks_to_remove
    if (come_before_container.contains(e.target)) {
        let id_label = e.target.id.replace('before', 'after');
        removed_task = come_after_container.querySelector(`label[for=${id_label}]`);
        other_tasks_to_remove = get_all_predecessors(removed_task.parentElement);
    }
    else {
        let id_label = e.target.id.replace('after', 'before');
        removed_task = come_before_container.querySelector(`label[for=${id_label}]`);
        other_tasks_to_remove = get_all_successors(removed_task.parentElement);
    }
    removed_task.querySelector('input').disabled=true;
    removed_task.style.display = "none";
    other_tasks_to_remove.forEach((task) =>{ task.style.display = "none"; task.disabled = true;});
}
function search_field_changed(e)
{
    let parent_container = come_before_container.contains(e.target) ? come_before_container : come_after_container;
    let choices = parent_container.querySelectorAll(".choice");
    choices.forEach((choice)=>
    {
        if(choice.querySelector("label").innerText.toLowerCase().indexOf(e.target.value.toLowerCase()) === -1)
        {
            choice.style.display = "none";
            choice.disabled= true;
        }
        else
        {
            choice.style.display= "block";
            choice.disabled = false;
        }
    });
}