from django.apps import AppConfig


class GpiCoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gpi_core'
