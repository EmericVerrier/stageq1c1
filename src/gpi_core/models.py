import datetime

from django.db import models
from django.utils.translation import gettext_lazy as _
from datetime import timedelta
from q1c1_django_utils import validators
from jours_feries_france import JoursFeries


class Agency(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=250)
    metropolis = models.CharField(max_length=100)
    acquisition_date = models.DateField('date Acquisition agence')
    validator_zip_code = validators.ZipCodeValidator()
    zip_code = models.CharField(null=True, max_length=5, validators=[validator_zip_code])

    def __str__(self):
        return f'{self.name}'


class GPI(models.Model):
    STATUS_OPEN = 'OPEN'
    STATUS_CLOSED = 'CLOSED'
    STATUS_NOT_STARTED = 'NOT_STARTED'
    STATUSES = (
        (STATUS_OPEN, _('Ouvert')),
        (STATUS_CLOSED, _('Fermé')),
        (STATUS_NOT_STARTED, _('Non commencé'))
    )

    protocol_signing_date = models.DateField('date de signature du protocole')
    definitive_signing_date = models.DateField('date de signature définitive')
    agency = models.OneToOneField(Agency, on_delete=models.CASCADE)
    earliest_start_date = models.DateField('date de début au plus tôt', null=True)
    latest_start_date = models.DateField('date de début au plus tard', null=True)
    earliest_end_date = models.DateField('date de fin au plus tôt', null=True)
    latest_end_date = models.DateField('date de fin au plus tard', null=True)
    status = models.TextField(choices=STATUSES)

    def compute_margins(self):
        """
        Calcul global des marges des tâches du GPI
        """
        list_first_tasks = self.tasks.filter(come_before__isnull=True)
        for task in list_first_tasks:
            task.recursive_margin_routine()

    def compute_latest_dates(self):
        """
        Calcul global des dates de début et de fin au plus tard des tâches du GPI
        """
        list_last_tasks = self.tasks.filter(come_after__isnull=True)
        if len(list_last_tasks) > 0:
            latest_end_date = max([t.earliest_end_date for t in list_last_tasks])
        for task in list_last_tasks:
            task.latest_end_date = latest_end_date
            task.recursive_latest_dates_routine()

    def compute_earliest_dates(self):
        """
        Calcul global des dates de début et de fin au plus tôt des tâches du GPI
        """
        list_first_tasks = self.tasks.filter(come_before__isnull=True)
        for task in list_first_tasks:
            task.recursive_earliest_dates_routine()

    def compute_h_ranks(self):  # h rank? depth/profondeur plutôt
        """

        Returns:

        """
        dict_h_ranks = {}
        # regarder prefetch pour loader les many foreignkeys
        cached_queryset = self.tasks.filter(come_before__isnull=True)
        for task in cached_queryset:
            task.recursive_rank_h_routine(dict_h_ranks=dict_h_ranks, cached_queryset=task)
        return dict_h_ranks

    def compute_v_ranks(self):
        """
        Calcul des positionnements verticaux des tâches selon leur nombre de successeurs
        cette méthode s'appuie sur une méthode définie dans la classe Task pour calculer les
        rangs et les hauteurs récursivement à partir des tâches enfants

        Returns :
        computed_tasks : un dictionnaire contenant les hauteurs des tâches
        """
        all_tasks = self.tasks.all()
        dict_visited_tasks = {task: [False, 0] for task in all_tasks}
        list_first_tasks = self.tasks.filter(come_before__isnull=True)
        if len(all_tasks) > 0:
            for first_task in list_first_tasks:
                first_task.height = first_task.rank_v = max([t.rank_v + 1 for t in list_first_tasks])
                first_task.recursive_rank_v_descent(tasks_explored=dict_visited_tasks, calling_task=first_task)
                dict_visited_tasks[first_task][1] = first_task.height
        return dict_visited_tasks


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Task(models.Model):
    STATUS_BLOCKED = 'BLOCKED'
    STATUS_WAITING = 'UNINITIATED'
    STATUS_IN_PROGRESS = 'ONGOING'
    STATUS_FINISHED = 'FINISHED'
    STATUSES = (
        (STATUS_BLOCKED, _('Bloquée')),
        (STATUS_WAITING, _('En attente')),
        (STATUS_IN_PROGRESS, _('En cours')),
        (STATUS_FINISHED, _('Terminée')),
    )

    description = models.CharField(max_length=250, null=True)
    name = models.CharField(max_length=50, null=False)
    real_end_date = models.DateField(null=True, blank=True)
    real_start_date = models.DateField(null=True, blank=True)
    earliest_start_date = models.DateField(null=True, blank=True)
    latest_start_date = models.DateField(null=True, blank=True)
    earliest_end_date = models.DateField(null=True, blank=True)
    latest_end_date = models.DateField(null=True, blank=True)
    come_before = models.ManyToManyField(to='self', symmetrical=False, related_name="come_after", blank=True)
    duration = models.DurationField(null=False)
    margin = models.DurationField(null=True)
    author = models.ForeignKey(null=True, to='InternalContributor', on_delete=models.CASCADE)
    category = models.ForeignKey(null=False, to='Category', on_delete=models.CASCADE)
    gpi = models.ForeignKey(null=False, to='GPI', on_delete=models.CASCADE, related_name='tasks')
    status = models.TextField(choices=STATUSES)

    def __init__(self, *args, **kwargs):
        super(Task, self).__init__(*args, **kwargs)
        self.rank_v = 0
        self.height = None
        self.rank_h = 0

    def __str__(self):
        return self.name

    def recursive_rank_v_descent(self, tasks_explored: dict, calling_task: "Task" = None):
        """Calcul des rangs et des hauteurs de chaque tâche pour pouvoir les positionner verticalement.

        Args :
            tasks_explored :
            calling_task :

        Returns:

        """
        suc_list = self.come_after.all()
        explored_suc = 0
        if calling_task is not None and self.height is None:
            self.height = calling_task.rank_v
        else:
            if self.height is None:
                self.height = 0
        tasks_explored[self][1] = self.height
        for suc in suc_list:
            if tasks_explored[suc][0] is True:
                explored_suc += 1
        if explored_suc == len(suc_list):  # si tous les sommets fils ont été explorés, alors, le sommet actuel est considéré comme exploré
            tasks_explored[self][0] = True
        if len(suc_list) == 0:  # si pas de successeurs, on met à jour notre rang et on le retourne incrémenté
            self.rank_v = calling_task.rank_v
            return self.rank_v + 1
        elif len(suc_list) > 0 and not tasks_explored[self][0]: # si présence de successeurs, on met à jour notre rang grâce au retour de l'appel récursif sur chacun des successeurs
            self.rank_v = calling_task.rank_v
            for suc in suc_list:
                self.rank_v = suc.recursive_rank_v_descent(tasks_explored=tasks_explored, calling_task=self)
            tasks_explored[self][0] = True
            tasks_explored[self][1] = self.height
            return self.rank_v + 1 # retour à l'appelant de la valeur du rang + 1 après avoir exploré tous les successeurs et mis à jour notre rang
        return self.rank_v + 1

    def recursive_margin_routine(self):
        """Routine de calcul récursif des marges disponibles sur une tâche,
        On fait la différence de dates entre la date de début au plus tard et la date de début au plus tôt.
        On devrait retrouver la même valeur en faisant la différence entre la date de fin au plus tard et la date de fin
        au plus tôt. Cette marge est enregistrée dans l'attribut margin. Il faut au préalable que les dates de début/fin
        au plus tôt/tard aient été calculées
        """
        suc_list = self.come_after.all()
        self.margin = self.latest_start_date - self.earliest_start_date
        self.save()
        for suc in suc_list:
            suc.recursive_margin_routine()

    def recursive_earliest_dates_routine(self):
        """Routine de calcul récursif des dates au plus tôt des tâches,
        on récupère la tâche dont la somme de la date de début au plus tôt et de la durée est maximale
        puis on boucle à partir de la durée de la tâche en jours en sautant les indices de jours qui sont compris dans
        les week-ends ou les jours fériés, on applique une procédure similaire pour la date de fin au plus tôt de la
        tâche
        correction manuelle, une tâche ne doit pas pouvoir se commencer sur un week-end ou un jour férié
        """
        pred_list = self.come_before.all()
        suc_list = self.come_after.all()
        one_day = timedelta(days=1)
        if len(pred_list) > 0:
            longest_pred_end_date = max(pred.earliest_end_date for pred in pred_list)
            longest_pred_end_date += one_day
            while longest_pred_end_date.isoweekday() > 5:
                longest_pred_end_date += one_day
            self.earliest_start_date = longest_pred_end_date

        days_index = 0
        loop_date = self.earliest_start_date
        while days_index < self.duration.days - 1:   # tant que l'on n'a pas ajouté tous les jours de durée
            # si rajouter un jour travaillé ne nous fait pas tomber sur le week-end, on le compte
            if (loop_date + one_day).isoweekday() < 6:
                days_index += 1
            loop_date += one_day
        self.earliest_end_date = loop_date
        self.save()
        for successor in suc_list:
            successor.recursive_earliest_dates_routine()

    def recursive_latest_dates_routine(self):
        """ Routine de calcul des dates au plus tard des tâches,
        si pas de successeurs, on récupère la date de fin au plus tôt avec une valeur maximale
        et on l'affecte à la tâche courante sinon, la date de fin au plus tard de la tâche courante
        est le minimum des dates de début au plus tard des successeurs.
        La date de début au plus tard est ensuite calculée comme : (date de fin au plus tard - durée de la tâche).
        On saute les jours non travaillés du calcul de la date.
        """
        pred_list = self.come_before.all()
        suc_list = self.come_after.all()
        one_day = timedelta(days=1)

        if len(suc_list) > 0:
            shortest_suc_start_date = min([t.latest_start_date for t in suc_list if t.latest_start_date is not None])
            shortest_suc_start_date -= one_day
            while shortest_suc_start_date.isoweekday() > 5:
                shortest_suc_start_date -= one_day
            self.latest_end_date = shortest_suc_start_date

        days_index = 0
        loop_date = self.latest_end_date

        while days_index < self.duration.days - 1:
            if (loop_date - one_day).isoweekday() < 6:
                days_index += 1
            loop_date -= one_day
        self.latest_start_date = loop_date
        self.save()
        for pred in pred_list:
            pred.recursive_latest_dates_routine()


class Contributor(models.Model):
    # stakeholder
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone_number = models.CharField(validators=[validators.validate_phone_number], max_length=10)
    email = models.CharField(validators=[validators.validate_email], max_length=254)


class InternalContributor(Contributor):
    pass


class Comment(models.Model):
    publication_date = models.DateTimeField(auto_now_add=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="comments")
    content = models.CharField(max_length=1000)
    author = models.ForeignKey(InternalContributor, on_delete=models.CASCADE, related_name="comments")
