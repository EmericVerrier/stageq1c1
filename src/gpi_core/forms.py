import datetime
from jours_feries_france import JoursFeries
from django.forms import ModelForm, ModelMultipleChoiceField, CheckboxSelectMultiple, DurationField
from django.forms.widgets import DateInput, NumberInput, Textarea
from gpi_core.models import GPI, Agency, Task, Comment
from django.core.exceptions import ValidationError


class GPIForm(ModelForm):
    class Meta:
        model = GPI

        # la section qui suit me parait très lourde
        protocol_signing_date_widget = DateInput()
        definitive_signing_date_widget = DateInput()
        protocol_signing_date_widget.input_type = "date"
        definitive_signing_date_widget.input_type = "date"

        fields = ("protocol_signing_date", "agency", "definitive_signing_date", "status")
        widgets = {
            "protocol_signing_date": protocol_signing_date_widget,
            "definitive_signing_date": definitive_signing_date_widget,
        }
        labels = {
            "status": "Statut",
            "agency": "Agence",
        }

    def clean(self):
        super().clean()
        if self.cleaned_data.get("protocol_signing_date") > self.cleaned_data.get("definitive_signing_date"):
            raise ValidationError(
                "la date de signature définitive ne peut pas être avant la date de signature du protocole"
            )


class AgencyForm(ModelForm):
    class Meta:
        model = Agency
        fields = "__all__"


class TaskForm(ModelForm):
    come_after = ModelMultipleChoiceField(queryset=Task.objects.all(), label="tâches suivantes",
                                          widget=CheckboxSelectMultiple, blank=True, required=False)

    def __init__(self, gpi, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.instance.gpi = gpi

    class Meta:
        model = Task

        exclude = ("earliest_end_date", "latest_end_date",
                   "latest_start_date", "author", "gpi",
                   "real_end_date", "real_start_date", "margin",
                   )
        labels = {
                  "description": "description",
                  "category": "catégorie", "name": "nom", "come_before": "tâches précédentes",
                  "earliest_start_date": "date de début prévue", "status": "statut de la tâche", "duration": "durée",
                    
                  }
        widgets = {"come_before": CheckboxSelectMultiple,
                   }

    def clean_duration(self):  # pas fameux du tout
        if self.cleaned_data["duration"].total_seconds() < 0:
            raise ValidationError("La durée d'une tâche ne peut pas être négative")
        self.cleaned_data["duration"] = datetime.timedelta(days=int(self.data.get("duration")))
        return self.cleaned_data.get("duration")

    def clean_earliest_start_date(self):
        if self.cleaned_data["earliest_start_date"]:
            if self.cleaned_data["earliest_start_date"].isoweekday() > 5:
                raise ValidationError("Une tâche ne peut pas commencer le week-end")
            if self.cleaned_data["earliest_start_date"] in JoursFeries.for_year(datetime.date.today().year).values():
                raise ValidationError("Une tâche ne peut pas commencer un jour férié")
        return self.cleaned_data.get("earliest_start_date")

    def clean(self):
        cleaned_data = super().clean()
        self.clean_earliest_start_date()
        self.clean_duration()
        precedents = cleaned_data.get("come_before")
        followers = cleaned_data.get("come_after")
        if precedents is not None and followers is not None:
            for selected_predecessor in precedents:
                if selected_predecessor in followers:
                    raise ValidationError("Une tâche ne peut pas à la fois être avant et après une autre tâche")
        return cleaned_data

    def save(self, commit=True):
        # double current_task, à voir la nécessité de ne pas commit
        if not self.errors:
            current_task = super(TaskForm, self).save(commit=True)
            precedents = self.cleaned_data.get("come_before")
            followers = self.cleaned_data.get("come_after")
            duration = self.cleaned_data.get("duration")
            current_task.duration = duration
            if followers is not None:
                for selected_successor in followers:
                    selected_successor.come_before.add(current_task)
            if precedents is not None:
                for selected_precedent in precedents:
                    current_task.come_before.add(selected_precedent)
            return current_task
        else:
            raise ValueError("Impossible de créer la tâche")


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        exclude = ("task", "author")
        labels = {"content": "commentaire"}
        widgets = {"content": Textarea}

