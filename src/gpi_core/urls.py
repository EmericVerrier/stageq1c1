from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),

    # Routes GPI
    path("gpi/create", views.create_gpi, name="create_gpi"),
    path("gpi/view/<int:gpi_id>", views.view_gpi, name="view_gpi"),

    # Routes Agence
    path("agency/create/", views.create_agency, name="create_agency"),

    # Routes Dashboard
    path("dashboard/", views.home, name="gpi_list"),
    path("dashboard/sorted/<str:criteria>/<str:order>/", views.sort_gpi, name="sort"),
    path("dashboard/search_<str:string_searched>/", views.search_gpi, name="search"),
    path("dashboard/get_all_gpi/", views.get_all_gpi, name="get_all_gpi"),

    # Routes Tâches
    path("task/update/gpi_<int:gpi_id>/task_<int:task_id>", views.view_task, name="view_task"),
    path("task/create/gpi_<int:gpi_id>", views.view_task, name="create_task"),
    path("task/delete/gpi_<int:gpi_id>/task_<int:task_id>", views.delete_task, name="delete_task"),

    # Routes Commentaires
    path("comment/create/task_<int:task_id>", views.create_comment, name="create_comment"),
    path("comment/update/task_<int:task_id>/comment_<int:comment_id>", views.update_comment, name="update_comment"),
    path("comment/delete/task_<int:task_id>/comment_<int:comment_id>", views.delete_comment, name="delete_comment"),

]
